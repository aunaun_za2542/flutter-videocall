
import '../extension/time.dart';

class RecentData {
  final String urlAvatar;
  final String userName;
  final String dateTime;

  RecentData({
    required this.urlAvatar,
    required this.userName,
    required this.dateTime,
  });
}

List<RecentData> recentList = [
  RecentData(
    urlAvatar: 'assets/inwza007-logo.png',
    userName: 'One Chat',
    dateTime: StringExtension.timeAgo("2021-10-06T12:45:26.507408+07:00"),
  ),
  RecentData(
    urlAvatar: 'assets/paperless-logo.png',
    userName: 'Paperless',
    dateTime: StringExtension.timeAgo("2022-10-06T11:48:26.507408+07:00"),
  ),
  RecentData(
    urlAvatar: 'assets/ga-logo.png',
    userName: 'Paperless',
    dateTime: StringExtension.timeAgo("2022-10-06T10:00:26.507408+07:00"),
  ),
  RecentData(
    urlAvatar: 'assets/unknown-logo.png',
    userName: 'Maneger',
    dateTime: StringExtension.timeAgo("2022-10-05T02:57:26.507408+07:00"),
  ),
  RecentData(
    urlAvatar: 'assets/unknown-logo.png',
    userName: 'Maneger',
    dateTime: StringExtension.timeAgo("2022-10-05T02:57:26.507408+07:00"),
  ),
  RecentData(
    urlAvatar: 'assets/unknown-logo.png',
    userName: 'Maneger',
    dateTime: StringExtension.timeAgo("2022-10-05T02:57:26.507408+07:00"),
  ),
  RecentData(
    urlAvatar: 'assets/unknown-logo.png',
    userName: 'Maneger',
    dateTime: StringExtension.timeAgo("2022-10-05T02:57:26.507408+07:00"),
  ),
  RecentData(
    urlAvatar: 'assets/unknown-logo.png',
    userName: 'Maneger',
    dateTime: StringExtension.timeAgo("2022-10-04T13:57:26.507408+07:00"),
  ),
  RecentData(
    urlAvatar: 'assets/unknown-logo.png',
    userName: 'Maneger',
    dateTime: StringExtension.timeAgo("2022-10-04T13:57:26.507408+07:00"),
  ),
  RecentData(
    urlAvatar: 'assets/unknown-logo.png',
    userName: 'Maneger',
    dateTime: StringExtension.timeAgo("2022-10-04T13:57:26.507408+07:00"),
  ),
  RecentData(
    urlAvatar: 'assets/unknown-logo.png',
    userName: 'Maneger',
    dateTime: StringExtension.timeAgo("2022-10-04T13:57:26.507408+07:00"),
  ),
  RecentData(
    urlAvatar: 'assets/unknown-logo.png',
    userName: 'Maneger',
    dateTime: StringExtension.timeAgo("2022-10-04T13:57:26.507408+07:00"),
  ),
  RecentData(
    urlAvatar: 'assets/unknown-logo.png',
    userName: 'Maneger',
    dateTime: StringExtension.timeAgo("2022-10-04T13:57:26.507408+07:00"),
  ),
];
