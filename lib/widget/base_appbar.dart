import 'package:flutter/material.dart';

class BaseAppBar extends StatefulWidget with PreferredSizeWidget {
  final String appbarTitle;

  const BaseAppBar({Key? key, required this.appbarTitle}) : super(key: key);

  @override
  State<BaseAppBar> createState() => _BaseAppBarState();

  @override
  // TODO: implement preferredSize
  Size get preferredSize => Size.fromHeight(kToolbarHeight);
}

class _BaseAppBarState extends State<BaseAppBar> {
  @override
  Widget build(BuildContext context) {
    return AppBar(
      titleSpacing: 0,
      centerTitle: false,
      automaticallyImplyLeading: false,
      title: Text(
        widget.appbarTitle,
        style: TextStyle(color: Colors.black,fontWeight: FontWeight.bold,fontSize: 18),
      ),
      leading: IconButton(
        onPressed: () {Navigator.pop(context);},
        icon: Icon(Icons.arrow_back,color: Colors.black,),
      ),
    );
  }
}
