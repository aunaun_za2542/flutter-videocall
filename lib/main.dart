import 'package:flutter/material.dart';
import 'package:videouiflutter/screens/launcher.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        theme:  ThemeData(
          appBarTheme: AppBarTheme(
            color: Colors.white,elevation: 0,
          ),),
      home: Launcher(),
    );
  }
}