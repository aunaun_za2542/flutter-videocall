import 'package:flutter/material.dart';
import 'package:group_list_view/group_list_view.dart';

import '../extension/time.dart';

class KeypadPage extends StatefulWidget {
  const KeypadPage({Key? key}) : super(key: key);

  @override
  State<KeypadPage> createState() => _KeypadPageState();
}

class _KeypadPageState extends State<KeypadPage> {
//   @override
//   Widget build(BuildContext context) {
//     return Scaffold(
//       body: Center(
//         child: Container(
//           child: Text('Keypad Page'),
//         ),
//       ),
//     );
//   }
// }

  final recentList = [
    {
      'group': StringExtension.timeAgo("2022-10-09T12:45:26.507408+07:00"),
      'avatar': 'assets/unknown-logo.png',
      'name': 'คุณเก๋',
    },
    {
      'group': StringExtension.timeAgo("2022-10-02T12:45:26.507408+07:00"),
      'avatar': 'assets/unknown-logo.png',
      'name': 'คุณอัน',
    },
    {
      'group': StringExtension.timeAgo("2022-10-01T12:45:26.507408+07:00"),
      'avatar': 'assets/unknown-logo.png',
      'name': 'คุณกล้าม',
    },
    {
      'group': StringExtension.timeAgo("2022-10-01T12:45:26.507408+07:00"),
      'avatar': 'assets/unknown-logo.png',
      'name': 'แพรววา',
    },
    {
      'group': StringExtension.timeAgo("2021-03-05T12:45:26.507408+07:00"),
      'avatar': 'assets/unknown-logo.png',
      'name': 'คุณเก๋',
    },
    {
      'group': StringExtension.timeAgo("2022-01-05T12:45:26.507408+07:00"),
      'avatar': 'assets/unknown-logo.png',
      'name': 'คุณอัน',
    },
    {
      'group': StringExtension.timeAgo("2022-10-04T12:45:26.507408+07:00"),
      'avatar': 'assets/unknown-logo.png',
      'name': 'คุณกล้าม',
    },
    {
      'group': StringExtension.timeAgo("2022-10-04T12:45:26.507408+07:00"),
      'avatar': 'assets/unknown-logo.png',
      'name': 'แพรววา',
    }
  ];

  @override
  Widget build(BuildContext context) {
    var elements = getGroupList();
    print(elements);
    return Scaffold(
      body: GroupListView(
        sectionsCount: elements.keys.toList().length,
        countOfItemInSection: (int section) {
          return elements.values.toList()[section].length;
        },
        groupHeaderBuilder: (BuildContext context, int section) {
          return Container(
            // color: Color(0xFFF4F6F7),
            color: Colors.grey.shade400,
            height: 40,
            child: Padding(
              padding: const EdgeInsets.fromLTRB(16, 8, 0, 8),
              child: Text(
                elements.keys.toList()[section],
                style: TextStyle(
                    fontSize: 16,
                    fontWeight: FontWeight.w400,
                    color: Colors.black),
              ),
            ),
          );
        },
        itemBuilder: (BuildContext context, IndexPath index) {
          var contactUser =
              elements.values.toList()[index.section][index.index];
          return Padding(
            padding: const EdgeInsets.symmetric(horizontal: 16),
            child: ExpansionTile(
              title: Row(
                children: [
                  CircleAvatar(
                    radius: 18,
                    backgroundImage: AssetImage(
                      contactUser["avatar"].toString(),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(left: 8),
                    child: Text(
                      contactUser["name"].toString(),
                      style:
                          TextStyle(fontSize: 16, fontWeight: FontWeight.w400),
                    ),
                  ),
                ],
              ),
              trailing: Text(''),
              children: [
                Container(
                  height: 48,
                  decoration: const BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.all(Radius.circular(10)),
                  ),
                  child: Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 16),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        InkWell(
                          onTap: () {
                            print('Call');
                          },
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.start,
                            children: [
                              // Icon(Icons.call),
                              Image.asset(
                                'assets/call-logo.png',
                                width: 32,
                                height: 40,
                              ),
                              Padding(
                                padding: const EdgeInsets.only(left: 8),
                                child: Text('Call'),
                              ),
                            ],
                          ),
                        ),
                        VerticalDivider(
                          thickness: 1,
                          indent: 10,
                          endIndent: 10,
                          color: Colors.grey,
                        ),
                        InkWell(
                          onTap: () {
                            print('Video Call');
                          },
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              // Icon(Icons.video_call),
                              Image.asset(
                                'assets/video-logo.png',
                                width: 32,
                                height: 40,
                              ),
                              Padding(
                                padding: const EdgeInsets.only(left: 8),
                                child: Text('Video Call'),
                              ),
                            ],
                          ),
                        ),
                        VerticalDivider(
                          thickness: 1,
                          indent: 10,
                          endIndent: 10,
                          color: Colors.grey,
                        ),
                        InkWell(
                          onTap: () {
                            print('Data');
                          },
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.end,
                            children: [
                              // Icon(Icons.account_circle_sharp),
                              Image.asset(
                                'assets/person-logo.png',
                                width: 32,
                                height: 40,
                              ),
                              Padding(
                                padding: const EdgeInsets.only(left: 8),
                                child: Text('Data'),
                              ),
                            ],
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              ],
            ),
          );
        },
        separatorBuilder: (context, index) => Padding(
          padding: const EdgeInsets.symmetric(horizontal: 16),
          child: Divider(
            color: Colors.grey.shade300,
            // color: Color(0xFFF2F2F2),
            thickness: 1,
            height: 1,
          ),
        ),
        sectionSeparatorBuilder: (context, section) => Container(),
      ),
    );
  }

  Map<String, List<Map<String, String>>> getGroupList() {
    var elements = <String, List<Map<String, String>>>{};
    recentList
        .sort((a, b) => b["group"].toString().compareTo(a["group"].toString()));
    for (int i = 0; i < recentList.length; i++) {
      String key = recentList[i]["group"].toString();
      if (elements.containsKey(key)) {
        elements[key]?.add(recentList[i]);
      } else {
        elements[key] = [recentList[i]];
      }
    }
    return elements;
  }
}
