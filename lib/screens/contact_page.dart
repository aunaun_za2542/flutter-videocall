import 'package:flutter/material.dart';
import 'package:grouped_list/grouped_list.dart';
import 'package:intl/intl.dart';
import '../extension/time.dart';

class ContactPage extends StatefulWidget {
  const ContactPage({Key? key}) : super(key: key);

  @override
  State<ContactPage> createState() => _ContactPageState();
}

class _ContactPageState extends State<ContactPage> {
  List<dynamic> contactList = [
    {
      'group': "2022-10-09T12:45:26.507408+07:00",
      'avatar': 'assets/unknown-logo.png',
      'name': 'คุณเก๋',
    },
    {
      'group': "2022-10-03T12:45:26.507408+07:00",
      'avatar': 'assets/unknown-logo.png',
      'name': 'คุณอัน',
    },
    {
      'group': "2022-10-01T12:45:26.507408+07:00",
      'avatar': 'assets/fdfasf-logo.png',
      'name': 'คุณกล้าม',
    },
    {
      'group': "2022-10-04T12:45:26.507408+07:00",
      'avatar': 'assets/ga-logo.png',
      'name': 'แพรววา',
    },
    {
      'group': "2021-03-06T12:45:26.507408+07:00",
      'avatar': 'assets/unknown-logo.png',
      'name': 'คุณเก๋',
    },
    {
      'group': "2022-01-05T12:45:26.507408+07:00",
      'avatar': 'assets/unknown-logo.png',
      'name': 'คุณอัน',
    },
    {
      'group': "2021-04-04T12:45:26.507408+07:00",
      'avatar': 'assets/unknown-logo.png',
      'name': 'คุณกล้าม',
    },
    {
      'group': "2022-10-04T12:45:26.507408+07:00",
      'avatar': 'assets/asdf-logo.png',
      'name': 'แพรววา',
    }
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: GroupedListView<dynamic, String>(
        groupBy: (element) => DateFormat("yyyy-MM-dd")
            .format(DateTime.parse(element['group']).toLocal()),
        elements: contactList,
        groupComparator: (value1, value2) => value2.compareTo(value1),
        groupSeparatorBuilder: (value) {
          return Container(
            color: Colors.grey.shade400,
            child: Padding(
              padding: const EdgeInsets.fromLTRB(16, 8, 0, 8),
              child: Text(
                StringExtension.timeAgo(value),
                style: TextStyle(fontWeight: FontWeight.w400, fontSize: 16),
              ),
            ),
          );
        },
        itemBuilder: (context, element) {
          return Container(
            child: Column(children: [
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 16),
                child: Container(
                  decoration: BoxDecoration(
                    border: context == 0
                        ? const Border() // This will create no border for the first item
                        : Border(
                            top: BorderSide(
                            width: 1,
                            color: Colors.grey.shade300,
                          )), // This will create top borders for the rest
                  ),
                ),
              ),
              Theme(
                data: Theme.of(context)
                    .copyWith(dividerColor: Colors.transparent),
                child: ExpansionTile(
                  // trailing: Text('${element.date.hour}:00'),
                  trailing: Text(''),
                  title: Container(
                    padding: EdgeInsets.symmetric(horizontal: 16),
                    child: Row(
                      children: [
                        Image.asset(
                          element['avatar'],
                          width: 40,
                          errorBuilder: (context, error, stackTrace) {
                            return CircleAvatar(
                                radius: 20,
                                backgroundImage:
                                    AssetImage('assets/unknown-logo.png'));
                          },
                        ),
                        Padding(
                          padding: const EdgeInsets.only(left: 8),
                          child: Text(element['name']),
                        )
                      ],
                    ),
                  ),
                  // trailing: Text(element['name']),
                  children: [
                    Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 16),
                      child: Container(
                        height: 48,
                        decoration: BoxDecoration(
                          color: Colors.white,
                          borderRadius: BorderRadius.circular(8),
                          boxShadow: [
                            BoxShadow(
                              color: Color(0xffDDDDDD),
                              blurRadius: 1.0,
                              spreadRadius: 1.0,
                            )
                          ],
                        ),
                        child: Padding(
                          padding: const EdgeInsets.symmetric(horizontal: 16),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              InkWell(
                                onTap: () {
                                  print('Call');
                                },
                                child: Row(
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  children: [
                                    Image.asset(
                                      'assets/call-logo.png',
                                      width: 32,
                                      height: 40,
                                    ),
                                    // Icon(Icons.call),
                                    Padding(
                                      padding: const EdgeInsets.only(left: 8),
                                      child: Text('Call'),
                                    ),
                                  ],
                                ),
                              ),
                              VerticalDivider(
                                thickness: 1,
                                indent: 10,
                                endIndent: 10,
                                color: Colors.grey,
                              ),
                              InkWell(
                                onTap: () {
                                  print('Video Call');
                                },
                                child: Row(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: [
                                    Image.asset(
                                      'assets/video-logo.png',
                                      width: 32,
                                      height: 40,
                                    ),
                                    // Icon(Icons.video_call),
                                    Padding(
                                      padding: const EdgeInsets.only(left: 8),
                                      child: Text('Video Call'),
                                    ),
                                  ],
                                ),
                              ),
                              VerticalDivider(
                                thickness: 1,
                                indent: 10,
                                endIndent: 10,
                                color: Colors.grey,
                              ),
                              InkWell(
                                onTap: () {
                                  print('Data');
                                },
                                child: Row(
                                  mainAxisAlignment: MainAxisAlignment.end,
                                  children: [
                                    Image.asset(
                                      'assets/person-logo.png',
                                      width: 32,
                                      height: 40,
                                    ),
                                    // Icon(Icons.account_circle_sharp),
                                    Padding(
                                      padding: const EdgeInsets.only(left: 8),
                                      child: Text('Data'),
                                    ),
                                  ],
                                ),
                              ),
                            ],
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ]),
          );
        },
      ),
    );
  }
}
