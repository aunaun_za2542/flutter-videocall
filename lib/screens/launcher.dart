import 'package:flutter/material.dart';
import 'package:videouiflutter/screens/contact_page.dart';
import 'package:videouiflutter/screens/keypad_page.dart';
import 'package:videouiflutter/screens/recent_page.dart';
import 'package:videouiflutter/widget/base_appbar.dart';

class Launcher extends StatefulWidget {
  const Launcher({Key? key}) : super(key: key);

  @override
  State<StatefulWidget> createState() {
    return _LauncherState();
  }
}

class _LauncherState extends State<Launcher> {
  int selectedIndex = 0;
  static const List<Widget> _widgetOptions = <Widget>[
    RecentPage(),
    KeypadPage(),
    ContactPage()
  ];

  void onItemTapped(int index) {
    setState(() {
      selectedIndex = index;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: BaseAppBar(appbarTitle: 'Call and video call',),
      body: Center(
        child: _widgetOptions.elementAt(selectedIndex),
      ),
      bottomNavigationBar: BottomNavigationBar(
        items: const <BottomNavigationBarItem>[
          BottomNavigationBarItem(
            icon: Icon(Icons.access_time),
            label: 'Recents',
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.apps_outlined),
            label: 'Keypad',
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.account_circle_outlined),
            label: 'Contacts',
          ),
        ],
        currentIndex: selectedIndex,
        // selectedItemColor: Colors.amber[800],
        onTap: onItemTapped,
      ),
    );
  }
}
