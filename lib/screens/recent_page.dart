import 'package:flutter/material.dart';
import 'package:videouiflutter/components/recent_list_data.dart';

class RecentPage extends StatefulWidget {
  const RecentPage({Key? key}) : super(key: key);

  @override
  State<RecentPage> createState() => _RecentPageState();
}

class _RecentPageState extends State<RecentPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Padding(
            padding: const EdgeInsets.all(16.0),
            child: Text(
              'Recents',
              style: TextStyle(fontSize: 20, fontWeight: FontWeight.w700),
            ),
          ),
          Expanded(
            child: ListView.separated(
              itemCount: recentList.length,
              itemBuilder: (context, index) {
                recentList.sort((a, b) =>
                    b.dateTime.toString().compareTo(a.dateTime.toString()));
                return Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 16),
                  child: Theme(
                    data: Theme.of(context)
                        .copyWith(dividerColor: Colors.transparent),
                    child: ExpansionTile(
                      title: Row(
                        children: [
                          Image.asset(
                            recentList[index].urlAvatar,
                            width: 40,
                            errorBuilder: (context, error, stackTrace) {
                              return CircleAvatar(
                                  radius: 20,
                                  backgroundImage:
                                      AssetImage('assets/unknown-logo.png'));
                            },
                          ),
                          Padding(
                            padding: const EdgeInsets.only(left: 8),
                            child: Text(recentList[index].userName),
                          ),
                        ],
                      ),
                      trailing: Text(recentList[index].dateTime),
                      children: [
                        Container(
                          height: 48,
                          decoration: BoxDecoration(
                            color: Colors.white,
                            borderRadius: BorderRadius.circular(4),
                            boxShadow: [
                              BoxShadow(
                                color: Color(0xffDDDDDD),
                              )
                            ],
                          ),
                          child: Padding(
                            padding: const EdgeInsets.symmetric(horizontal: 16),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                InkWell(
                                  onTap: () {
                                    print('Call');
                                  },
                                  child: Row(
                                    mainAxisAlignment: MainAxisAlignment.start,
                                    children: [
                                      Image.asset(
                                        'assets/call-logo.png',
                                        width: 32,
                                        height: 40,
                                      ),
                                      // Icon(Icons.call),
                                      Padding(
                                        padding: const EdgeInsets.only(left: 8),
                                        child: Text('Call'),
                                      ),
                                    ],
                                  ),
                                ),
                                VerticalDivider(
                                  thickness: 1,
                                  indent: 10,
                                  endIndent: 10,
                                  color: Colors.grey,
                                ),
                                InkWell(
                                  onTap: () {
                                    print('Video Call');
                                  },
                                  child: Row(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: [
                                      Image.asset(
                                        'assets/video-logo.png',
                                        width: 32,
                                        height: 40,
                                      ),
                                      // Icon(Icons.video_call),
                                      Padding(
                                        padding: const EdgeInsets.only(left: 8),
                                        child: Text('Video Call'),
                                      ),
                                    ],
                                  ),
                                ),
                                VerticalDivider(
                                  thickness: 1,
                                  indent: 10,
                                  endIndent: 10,
                                  color: Colors.grey,
                                ),
                                InkWell(
                                  onTap: () {
                                    print('Data');
                                  },
                                  child: Row(
                                    mainAxisAlignment: MainAxisAlignment.end,
                                    children: [
                                      Image.asset(
                                        'assets/person-logo.png',
                                        width: 32,
                                        height: 40,
                                      ),
                                      // Icon(Icons.account_circle_sharp),
                                      Padding(
                                        padding: const EdgeInsets.only(left: 8),
                                        child: Text('Data'),
                                      ),
                                    ],
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                );
              },
              separatorBuilder: (context, index) => Padding(
                padding: const EdgeInsets.symmetric(horizontal: 16),
                child: Divider(
                  color: Colors.grey.shade300,
                  // color: Color(0xFFF2F2F2),
                  thickness: 1,
                  height: 1,
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
