import 'package:intl/intl.dart';

extension StringExtension on String {

  static String timeAgo(String date) {
    var parsedDate = DateTime.parse(date);
    var localDate = parsedDate.toLocal();
    String formattedDateDayMonthYear = DateFormat('dd MMM yyyy').format(
        localDate);
    // String formattedDateDay = DateFormat('EEE d').format(localDate);
    String formattedDateDay = DateFormat('EEEEE').format(localDate);
    String formattedDateToday = DateFormat('hh:mm').format(localDate);
    Duration diff = DateTime.now().difference(parsedDate);
    // if (diff.inDays > 365) return "${(diff.inDays / 365).floor()} ปีที่แล้ว";
    // if (diff.inDays > 30) return "${(diff.inDays / 30).floor()} เดือนที่แล้ว";
    // if (diff.inDays > 0 && diff.inDays <=6) return "${diff.inDays} วันที่แล้ว";
    // if (diff.inHours > 0 && diff.inHours < 24) return '${diff.inHours} ชั่วโมงที่แล้ว';
    if (diff.inMinutes > 0 && diff.inMinutes < 60)
      return '${diff.inMinutes} นาทีที่แล้ว';
    // if (diff.inHours > 0 && diff.inHours < 24) return formattedDateToday;
    if (diff.inHours > 0 && diff.inHours < 24) return 'Today';
    if (diff.inHours >= 24 && diff.inHours < 48) return 'Yesterday';
    if (diff.inDays >= 2 && diff.inDays < 7) return formattedDateDay;
    if (diff.inDays >= 7) return formattedDateDayMonthYear;
    return "ตอนนี้";
  }
}
// static String convertMessageDatetime(String date){
//   var parsedDate = DateTime.parse(date);
//   var localDate = parsedDate.toLocal();
//   String formattedDate = DateFormat('HH:mm').format(localDate);
//   return formattedDate;
// }

// static bool diff(String date) {
//   if(date != "") {
//     var parsedDate = DateTime.parse(date.split("T")[0]);
//     var localDate = parsedDate.toLocal();
//     Duration diff = DateTime.now().difference(parsedDate);
//     if (diff.inDays > 0) {
//       return true;
//     } else {
//       return false;
//     }
//   }
//   return false;
// }

